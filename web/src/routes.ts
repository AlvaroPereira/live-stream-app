import express from "express";
import HomepageController from "./presentation/controllers/HomepageController";
import PingController from "./presentation/controllers/PingController";

const router = express.Router();

router.get("/", HomepageController.index);
router.get("/v1/ping", PingController.index);

export default router;