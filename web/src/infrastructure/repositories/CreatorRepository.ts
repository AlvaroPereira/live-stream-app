import { RowDataPacket } from "mysql2/promise";
import Creator from "../../domain/Creator";
import { pool } from "../utils/MySqlPool";

export default class CreatorRepository {
    static async getCreators(offset: number, limit: number): Promise<Array<Creator>> {

        const creators = new Array<Creator>();

        await pool.query("SELECT id, name FROM creators LIMIT ? OFFSET ?", [limit, offset]).then(results => {
            const data: RowDataPacket[] = results.shift() as RowDataPacket[];

            data.map((item: RowDataPacket): void => {
                let creator: Creator = new Creator();
                creator.id = item.id;
                creator.name = item.name;
                creators.push(creator);
            });
        })

        return creators;
    }

    static async countCreators(): Promise<number> {
        let count: number = 0;

        await pool.query("SELECT COUNT(1) AS count FROM creators", []).then(results => {
            const data: RowDataPacket[] = results.shift() as RowDataPacket[];

            data.map((item: RowDataPacket): void => {
               count = item.count;
            });
        });

        return count;
    }
}