import express, { Express } from "express";
import routes from "./routes";
import path from "path";

const app: Express = express();

app.use(routes);

app.set("view engine", "pug");

app.set("views", path.join(__dirname, "presentation/views"));

export default app;