export default class PaginatedResponse<T> {
    private readonly _items: Array<T>;
    private readonly _currentPage: number;
    private readonly _totalItems: number;
    private readonly _totalPages: number;
    private readonly _limit: number;

    constructor(items: Array<T>, currentPage: number, totalItems: number, totalPages: number, limit: number) {
        this._items = items;
        this._currentPage = currentPage;
        this._totalItems = totalItems;
        this._totalPages = totalPages;
        this._limit = limit;
    }

    get hasNext(): boolean {
        return this._currentPage < this._totalPages;
    }

    get hasPrevious(): boolean {
        return this._currentPage > 1;
    }

    get nextPage(): number | null {
        return this.hasNext ? this._currentPage + 1 : null;
    }

    get previousPage(): number | null {
        return this.hasPrevious ? this._currentPage - 1 : null;
    }

    get items(): Array<T> {
        return this._items;
    }

    get currentPage(): number {
        return this._currentPage;
    }

    get totalItems(): number {
        return this._totalItems;
    }

    get totalPages(): number {
        return this._totalPages;
    }

    get limit(): number {
        return this._limit;
    }
}