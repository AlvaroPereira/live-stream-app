import app from "../app";

const port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log(`The web app was started on port ${port}`);
})