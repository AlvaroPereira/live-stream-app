import { Request, Response } from "express";

export default class PingController {
    static async index(request: Request, response: Response): Promise<Response<Record<string, any>>> {
        return response.status(200).json({
            pong: true
        });
    }
}