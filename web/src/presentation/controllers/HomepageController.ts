import { Request, Response } from "express";

export default class HomepageController {
    static index(request: Request, response: Response): void {
        return response.render("homepage/index");
    }
}