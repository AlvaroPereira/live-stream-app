import * as crypto from "crypto";
import Creator from "../../src/domain/Creator";

describe('Creator', () => {
    test("Should create a Creator entity correctly", () => {

        const creatorId: string = crypto.randomUUID();
        const creatorName: string = "John Doe";

        let creator = new Creator();
        creator.id = creatorId;
        creator.name = creatorName;

        expect(creator.id).toEqual(creatorId);
        expect(creator.name).toEqual(creatorName);
    })
})