import * as crypto from "crypto";
import PaginatedResponse from "../../src/app/responses/PaginatedResponse";

describe("PaginatedResponse", () => {
    test("Should instantiate a PaginatedResponse with correct properties when it is called", (): void => {

        const items: Array<string> = [];

        for (let i = 0; i < 5; i++) {
            items.push(crypto.randomBytes(15).toString("hex"));
        }

        const totalPages: number = 1;
        const currentPage: number = 1;
        const totalItems: number = items.length;
        const limit: number = 10;

        const paginatedResponse: PaginatedResponse<string> = new PaginatedResponse(items, currentPage, totalItems, totalPages, limit);

        expect(paginatedResponse.hasPrevious).toBe(false);
        expect(paginatedResponse.hasNext).toBe(false);
        expect(paginatedResponse.items).toEqual(items);
        expect(paginatedResponse.currentPage).toBe(currentPage);
        expect(paginatedResponse.totalItems).toBe(totalItems);
        expect(paginatedResponse.totalPages).toBe(totalPages);
        expect(paginatedResponse.limit).toBe(limit);
        expect(paginatedResponse.nextPage).toBe(null);
        expect(paginatedResponse.previousPage).toBe(null);
    });

    test("Should indicate hasNext and hasPrevious correctly", (): void => {
        const items: Array<string> = ["item1", "item2"];
        const totalPages: number = 3;
        const currentPage: number = 2;
        const totalItems: number = 6;
        const limit: number = 2;

        const paginatedResponse: PaginatedResponse<string> = new PaginatedResponse(items, currentPage, totalItems, totalPages, limit);

        expect(paginatedResponse.hasPrevious).toBe(true);
        expect(paginatedResponse.hasNext).toBe(true);
        expect(paginatedResponse.nextPage).toBe(3);
        expect(paginatedResponse.previousPage).toBe(1);
    });

    test("Should handle edge cases for pagination", (): void => {
        const items: Array<string> = ["item1"];
        const totalPages: number = 1;
        const currentPage: number = 1;
        const totalItems: number = 1;
        const limit: number = 1;

        const paginatedResponse: PaginatedResponse<string> = new PaginatedResponse(items, currentPage, totalItems, totalPages, limit);

        expect(paginatedResponse.hasPrevious).toBe(false);
        expect(paginatedResponse.hasNext).toBe(false);
        expect(paginatedResponse.nextPage).toBe(null);
        expect(paginatedResponse.previousPage).toBe(null);
    });
});