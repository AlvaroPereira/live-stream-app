import * as crypto from "crypto";
import { pool } from "../../src/infrastructure/utils/MySqlPool";
import CreatorRepository from "../../src/infrastructure/repositories/CreatorRepository";
import Creator from "../../src/domain/Creator";

describe("CreatorRepository", () => {

    beforeAll(async () => {
        await pool.query(`
            CREATE TABLE IF NOT EXISTS creators (
                id CHAR(36) NOT NULL PRIMARY KEY,
                name VARCHAR(50) NOT NULL
            );
        `);
    });

    beforeEach(async () => {
        await pool.query("DELETE FROM creators;");
        for (let i = 0; i < 10; i++) {
            const creatorId: string = crypto.randomUUID();
            const creatorName: string = crypto.randomBytes(15).toString("hex");
            await pool.query("INSERT INTO creators SET id = ?, name = ?", [creatorId, creatorName]);
        }
    });

    afterAll(async () => {
        await pool.query("DROP TABLE IF EXISTS creators;");
    });

    test("should retrieve a list of creators when getCreators is called with pagination", async (): Promise<void> => {
        const creators: Array<Creator> = await CreatorRepository.getCreators(0, 5);

        expect(creators.length).toEqual(5);
        expect(Array.isArray(creators)).toBe(true);
        expect(creators[0]).toBeInstanceOf(Creator);
    });

    test("should return an empty list when no creators are found", async (): Promise<void> => {
        await pool.query("DELETE FROM creators;");
        const creators: Array<Creator> = await CreatorRepository.getCreators(0, 5);

        expect(creators.length).toEqual(0);
        expect(Array.isArray(creators)).toBe(true);
    });

    test("should correctly count the number of creators when countCreators is called", async (): Promise<void> => {
        const count: number = await CreatorRepository.countCreators();

        expect(count).toEqual(10);
    });

    test("should return zero when no creators are found in countCreators", async (): Promise<void> => {
        await pool.query("DELETE FROM creators;");
        const count: number = await CreatorRepository.countCreators();

        expect(count).toEqual(0);
    });

    test("should retrieve the correct creators when getCreators is called with different pagination parameters", async (): Promise<void> => {
        const firstBatch: Array<Creator> = await CreatorRepository.getCreators(0, 5);
        const secondBatch: Array<Creator> = await CreatorRepository.getCreators(5, 5);

        expect(firstBatch.length).toEqual(5);
        expect(secondBatch.length).toEqual(5);
        expect(firstBatch[0].id).not.toEqual(secondBatch[0].id);
    });
});