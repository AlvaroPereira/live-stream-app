import { exec as execCallback } from "child_process";
import { promisify } from "util";
import {pool} from "./src/infrastructure/utils/MySqlPool";

const exec = promisify(execCallback);

export default async function globalSetup(): Promise<void> {
    await exec("docker pull mysql:8");
    await exec(`docker run --name live-stream-app-mysql -e MYSQL_ROOT_PASSWORD=${process.env.MYSQL_PASSWORD} -e MYSQL_DATABASE=${process.env.MYSQL_DATABASE_NAME} -p 3306:3306 -d mysql:8`);

    while (true) {
        try {
            await pool.query("SELECT 1");
            break;
        } catch {
            await new Promise(res => setTimeout(res, 2000));
        }
    }
}