import { exec as execCallback } from "child_process";
import { promisify } from "util";

const exec = promisify(execCallback);

export default async function globalTearDown(): Promise<void> {
    await exec("docker stop live-stream-app-mysql");
    await exec("docker rm live-stream-app-mysql");
    await exec("docker rmi mysql:8");
}